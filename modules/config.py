#!/usr/bin/python3

"""
 Space shooter game

 Constants module

"""

import pygame

# Game speed
ANI = 20
FPS = 60

# Define some colors
BLACK = [0, 0, 0]
GREEN = [0, 255, 0]
WHITE = [255, 255, 255]

# How many pixels the player ship moves
steps = 5

# Placeholder for score
score = 0

# Power ups enabled?
ship_power_up_1 = False

# Set the width and height of the screen [width, height]
RESOLUTION = [800, 600]
screen = pygame.display.set_mode(RESOLUTION)
pygame.display.set_caption("Dark Oblivion")

# Sprite lists
all_sprites_list = pygame.sprite.Group()
star_list = []
bullet_list = pygame.sprite.Group()
powerup_list = pygame.sprite.Group()
