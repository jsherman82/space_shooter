#!/usr/bin/python3

"""
 Space shooter game

 Player Ship module

"""

import os
import pygame

# Custom modules
from . import config as c

class Ship(pygame.sprite.Sprite):
    '''
    Spawn a player ship
    '''
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.movex = 0
        self.movey = 0
        self.frame = 0
        self.images = []
        for i in range(1,4):
            img = pygame.image.load(os.path.join('resources','images','ship' + str(i) + '.png')).convert_alpha()
            self.images.append(img)
            self.image = self.images[0]
            self.rect = self.image.get_rect()

    def control(self,x,y):
        '''
        control player movement
        '''
        self.movex += x
        self.movey += y

    def update(self):
        '''
        update sprite position
        '''
        self.rect.x = self.rect.x + self.movex
        self.rect.y = self.rect.y + self.movey

        # moving left
        if self.movex < 0:
            self.frame += 1
            if self.frame > 2*c.ANI:
                self.frame = 0
            self.image = self.images[self.frame//c.ANI]

        # moving right
        if self.movex > 0:
            self.frame += 1
            if self.frame > 2*c.ANI:
                self.frame = 0
            self.image = self.images[self.frame//c.ANI]
