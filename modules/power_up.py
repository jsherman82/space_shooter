#!/usr/bin/python3
"""
Space shooter game
Power up module
"""

from modules import config as c
import pygame
import random

class Multi_Shot(pygame.sprite.Sprite):
    """
    This class represents the Multi Shot powerup
    It derives from the "Sprite" class in Pygame
    """
 
    def __init__(self, color, width, height):
        """ Constructor. Pass in the color of the block,
        and its x and y position. """
        # Call the parent class (Sprite) constructor
        super().__init__()
 
        # Create an image of the block, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.image = pygame.Surface([width, height])
        self.image.fill(color)
 
        # Fetch the rectangle object that has the dimensions of the image
        # image.
        # Update the position of this object by setting the values
        # of rect.x and rect.y
        self.rect = self.image.get_rect()
 
        # Instance variables that control the edges of where we bounce
        self.left_boundary = 0
        self.right_boundary = 0
        self.top_boundary = 0
        self.bottom_boundary = 0
 
        # Instance variables for our current speed and direction
        self.change_x = 0
        self.change_y = 0
 
    def update(self):
        """ Called each frame. """
        self.rect.y += self.change_y
 
        if self.rect.bottom >= self.bottom_boundary:
            self.kill()

def draw_power_ups():
    
    powerup_chance = random.randint(1, 100)

    if powerup_chance == 50:
        # This represents a powerup item
        powerup = Multi_Shot(c.GREEN, 20, 15)
 
        # Set a random location for the block
        powerup.rect.x = random.randrange(c.RESOLUTION[0])
        #block.rect.y = random.randrange(c.RESOLUTION[1])
 
        powerup.change_y = 5
        powerup.left_boundary = 0
        powerup.top_boundary = 0
        powerup.right_boundary = c.RESOLUTION[0]
        powerup.bottom_boundary = c.RESOLUTION[1]
 
        # Add the block to the list of objects
        c.powerup_list.add(powerup)
        c.all_sprites_list.add(powerup)
